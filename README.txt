Comment faire fonctionner le projet :

	-	Installer Docker : https://docs.docker.com/desktop/windows/install/
	-	faire un git clone du projet.
	
	-	faire un "docker compose up -d" du projet
	-	faire un "docker exec -it www_docker_symfony bash" puis "cd project"
	-	faire un "composer install"
	-	faire un "php bin/console doctrine:database:create"
	-	faire un "php bin/console doctrine:migrations:migrate"
	-	faire un "php bin/console doctrine:fixtures:load"

acceder au site sur votre navigateur à l'adresse 127.0.0.1:8741
acceder à la BDD sur votre navigateur à l'adresse 127.0.0.1:8080 (user:root)(pas de mot de passe)