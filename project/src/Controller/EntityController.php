<?php

namespace App\Controller;

use App\Entity\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EntityController extends AbstractController
{
    #[Route('/entity', name: 'entity')]
    public function index(): Response
    {
        return $this->render('entity/index.html.twig', [
            'controller_name' => 'EntityController',
        ]);
    }

    public function read()
    {
        $entitys = $this->getDoctrine()->getRepository(Entity::class)->findAll();

        return $this->render('entity/read.html.twig', 
        ['entitys' => $entitys,
         'controller_name' => 'EntityController'] );
    }

    public function addone(Request $request, $id){

        $entitys = $this->getDoctrine()->getRepository(Entity::class)->findAll();

        $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy(
            ['id' => $id]
        );
        $entity->addOne();

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $this->render('entity/read.html.twig', 
        ['entitys' => $entitys,
         'controller_name' => 'EntityController']);
    }

    public function removeone(Request $request, $id){

        $entitys = $this->getDoctrine()->getRepository(Entity::class)->findAll();

        $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy(
            ['id' => $id]
        );
        $entity->removeOne();

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $this->render('entity/read.html.twig', 
        ['entitys' => $entitys,
         'controller_name' => 'EntityController']);
    }
}
