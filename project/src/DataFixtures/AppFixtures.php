<?php

namespace App\DataFixtures;

use App\Entity\Entity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $entity1 = new Entity();
            $entity1->setTitle("first");
            $entity1->setNumber(3);
        
        
        $entity2 = new Entity();
            $entity2->setTitle("second");
            $entity2->setNumber(12);



    
        $manager->persist($entity1);
        $manager->persist($entity2);

        $manager->flush();
    }
}
